# @takatanjs/auth

modules for users-roles-access management

module requires @takatanjs/orm (database) to hold data, creates required collections (user, role, usersgroup), and methods for permissions checking.

## Roles

Roles are templatable strings, with format `module.permission`.
Roles with `*.root` permission gives full access to `*` module permissions

There are 2 default permissions, installed by module: `core.root` and `auth.root`

### core.root

This is special role, gives all access to any requested permission

## UsersGroups

Unite roles and groups. To provide permission 'x.y' to user `A` some UsersGroup `G` should be choosed (or created) and role `x.y` should be added to `G` roles, and `A` should be added to `G` users.

There are 2 groups created by module defaults:
 - Administrators - system group, automatically updated with all existing roles
 - Guests - system group, by default without any roles. Roles provided to this group will be automatically granted to "guests users"

## Users

By default users have Username and Password fields. Also user has Created and UUID fields, empty by default. use @takatanjs/corex for generators

Only one user `root` with password `1234` created by default, and pushed to Administrators UsersGroup

## Password field

Password field can be used to safe store passwords and verify them


under development
