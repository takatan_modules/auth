const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('UsersGroup')

class UsersGroup extends Takatan.extends('Node')
{
  /*
  addUser(user)
  {
    if (this.f('code')=='guests') return
    if (this.hasUser(user)) return
    this.field('users').push(user)
  }

  delUser(user)
  {
    if (this.f('code')=='guests') return
    if (!this.hasUser(user)) return
    this.field('users').filter(u=>!user.eq(u))
  }

  hasUser(user)
  {
    let fz = this.field('users').size()
    if (!fz) return false
    return this.field('users').reduce((acc,x)=>user.eq(x)?true:acc,false)
  }

  addRole(role)
  {
    if (this.hasRole(role)) return
    this.field('roles').push(role)
  }

  delRole(role)
  {
    if (!this.hasRole(role)) return
    this.field('roles').filter(r=>!role.eq(r))
  }

  hasRole(role)
  {
    let fz = this.field('roles').size()
    if (!fz) return false
    return this.field('roles').reduce((acc,x)=>role.eq(x)?true:acc,false)
  }
  */
}

module.exports = Takatan.register(UsersGroup)
