const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('UsersGroupModule')

const UsersGroup = require('./usersgroup')

class AuthUsersGroupModule extends Takatan.extends('AbstractCoreModule')
{

  moduleInit()
  {
    return Kefir.stream(obs=>{
      this.C('usersgroup').setNodeClass(UsersGroup)
      this.obsEnd(obs)
    })
  }

  moduleInstall()
  {
    return Kefir.stream(obs=>{
      //user groups bundle
      let bnd = this.B('usersgroup',true)
      bnd.name('Users Group')
      bnd.description('System users groups')
      bnd.addFlag('system')
      bnd.addField({ name:'Name', code: 'name', type: 'string', flags:['system','required','title']})
      bnd.addField({ name:'Code', code: 'code', type: 'string', flags:['system','required','unique','title']})
      bnd.addField({ name:'Description', code: 'description', type: 'string', flags:['system'], config:{ops:[{source:'manual',manual:true}]} })
      bnd.addField({ name:'Is system', code: 'isSystem', type: 'boolean', flags:['system','locked'], defaultValue: false })
      bnd.addField({ name:'Group users', code: 'users', type: 'relation', config: {ops:[{source:'manual',manual:'user'}]}, flags:['system'], quantity: 0 })
      bnd.addField({ name:'Group roles', code: 'roles', type: 'relation', config: {ops:[{source:'manual',manual:'role'}]}, flags:['system'], quantity: 0 })

      log.log('installing bundle',bnd.code())
      this.obsEnd(obs,bnd)
    })
    .flatMap(x=>x.save(true,true))
    .map(()=>{
      this.C('usersgroup').setNodeClass(UsersGroup)
      log.success('bundle installed')
    })
 
  }

}

module.exports = Takatan.register(AuthUsersGroupModule)
