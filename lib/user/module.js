const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('AuthUserModule')

const User = require('./user')

class AuthUserModule extends Takatan.extends('AbstractCoreModule')
{

  moduleInit()
  {
    return Kefir.stream(obs=>{
      this.C('user').setNodeClass(User)
      this.obsEnd(obs)
    })
  }


  moduleInstall()
  {
    return Kefir.stream(obs=>{

      //user bundle

      let b = this._core.module('bundle').bundle('user',true)
      b.name('User')
      b.description('System users')
      b.addFlag('system')
      b.addFlag('routed')
      b.addField({ name:'Username', code: 'username', type: 'string', flags: ['system','locked','title']})
      b.addField({ name: 'Password', code: 'password', type: 'password', flags:['system']})
      b.addField({ name: 'UUID', code: 'uuid', type: 'string', flags:['system','unique','required','locked'], generator:'uuid'})
      b.addField({ name: 'Created', code: 'created', type: 'datetime', flags:['system','logic','locked'], generator: 'now', config:{ops:[{source:'timezone',manual:''},{source:'manual',manual:['second','minute','hour','day','month','year']}]} })

      log.log('installing bundle',b.code())
      this.obsEnd(obs,b)
    })
    .flatMap(x=>x.save(true,true))
    .map(()=>{
      log.success('bundle installed')
      this.C('user').setNodeClass(User)
    })
  }

}

module.exports = Takatan.register(AuthUserModule)
