const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('User')

class User extends Takatan.extends('Node')
{

  constructor(...args)
  {
    super(...args)
    this._isGuest = false
    this._roles = []
    this._groups = []
  }

  isGuest()
  {
    return this._isGuest
  }

  userRoles()
  {
    return this._roles
  }

  userGroups()
  {
    return this._groups
  }

  toObject()
  {
    let ret = super.toObject()
    delete ret.password
    //ret.groups = this._groups.map(x=>x.toObject())
    ret.groups = this._groups
    ret.roles = this._roles.map(x=>x.toObject())
    return ret
  }
  /*
  saveUser()
  {
    return Rx.Observable.create(obs=>{
      let next = this.save()
      if (this.isGuest)
        next = Rx.of(1)
      next.subscribe(()=>{
        obs.next()
        obs.complete()
      })
    })
  }
  */

  populateAccess()
  {
    return this.M('auth').userAccess(this)
    .map(data=>{
      this._roles = data.roles
      this._groups = data.gcodes
      this._isGuest = true
      if (this._groups.length > 1)
      {
        this._isGuest = false
        this._groups = data.gcodes.filter(x=>x!='guests')
      }
      return this
    })
  }

  _can(role,doStrict=false)
  {
    let [mod,...xx] = role.split('.')
    if (!mod||!mod.length)
      mod = null
    return this._roles.reduce((acc,r)=>{
      //log.test('can?',[this.titles(),r.role()])
      if (r.role()=='core.root'&&!doStrict)
        return true
      if (mod&&r.f('module')==mod&&r.f('code')=='root'&&!doStrict)
        return true
      return r.role()==role?true:acc
    },false)
  }

  can(a,doStrict=false)
  {
    //console.log(['can?',this.titles(),this._roles])
    let list = [a]
    if (Array.isArray(a))
      list = a
    let ret = true
    list.forEach(rl=>{
      if (!this._can(rl,doStrict))
        ret = false
    })
    return ret
  }
  /*
  */

}

module.exports = Takatan.register(User)
