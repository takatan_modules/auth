const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('AuthRoleModule')

const Role = require('./role')

class AuthRoleModule extends Takatan.extends('AbstractCoreModule')
{

  moduleInit()
  {
    return Kefir.stream(obs=>{
      this.C('role').setNodeClass(Role)
      this.obsEnd(obs)
    })
  }


  moduleInstall()
  {
    return Kefir.stream(obs=>{
      //role bundle

      let bundle = this.B('role',true)
      bundle.name('Role')
      bundle.description('System roles')
      bundle.addFlag('system')
      bundle.addFlag('locked')
      bundle.addField({ name:'Module', code: 'module', type: 'string',flags: ['system','required','title'] })
      bundle.addField({ name:'Code', code: 'code', type: 'string', flags: ['system','required','title'] })
      bundle.addField({ name:'Description', code: 'description', type: 'string', flags: ['system']})

      log.log('installing bundle',bundle.code())
      this.obsEnd(obs,bundle)
    })
    .flatMap(x=>x.save(true,true))
    .map(()=>{
      this.C('role').setNodeClass(Role)
      log.success('bundle installed')
    })
  }

  roleSave(mdl,code,desc)
  {
    return this.C('role').findNode({$and:[{
      module: mdl
    },{
      code: code
    }]})
    .flatMap(role=>{
      log.log('saving role',[mdl,code])
      if (!role)
        role = this.C('role').newNode()
      //console.log('role',role,role.field('module'))
      role.field('module').set(mdl)
      role.field('code').set(code)
      if (typeof desc!='undefined')
        role.field('description').set(desc)
      return role.save()
    })
    map(role=>{
      log.success('role saved',role.role())
      return role
    })
  }

}

module.exports = Takatan.register(AuthRoleModule)
