const { Takatan } = require('@takatanjs/core')

class Role extends Takatan.extends('Node')
{

  role()
  {
    let mdl = this.f('module')
    let cde = this.f('code')
    return mdl+'.'+cde
  }

  toObject()
  {
    let ret = super.toObject()
    ret.role = this.role()
    return ret
  }

}

module.exports = Takatan.extends(Role)
