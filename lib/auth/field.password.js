const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('PasswordField')

class PasswordField extends Takatan.extends('AbstractField')
{


/*
   //schema is "any:mixed"
  static schema(info,sch={}) {
    sch.type = String
    return sch
  }
*/

  static info()
  {
    return {
      code: 'password',
      name: 'Password'
    }
  }

  verify(pwd,i=0)
  {
    let data = this._parent._model[this._code][i]
    if (!data || typeof data !== 'object' || typeof data.hash !== 'string' || typeof data.salt !== 'string' || !data.hash.length || !data.salt.length )
      return false
    return this.M('auth').saltVerify(pwd,data.hash,data.salt)
  }

  _v2db(v) {
    let ret = null
    //if(typeof v === 'undefined'||v===null) return null
    if(typeof v === 'string'&&!v.length) {
      v = this._lastDbV
    }
    if(typeof v === 'string'&&v.length) {
      ret = {
        hash: '',
        salt: this.M('auth').genSalt()
      }
      ret.hash = this.M('auth').salt(v,ret.salt)
    }
    if(typeof v === 'object' && typeof v.hash === 'string' && typeof v.salt === 'string')
      ret = v
    //console.log('push password to db',v,ret)
    return ret
  }

  _db2v(db) {
    //console.log('restore password from db',db)
    this._lastDbV = db
    if(typeof db ==='undefinded'||db===null) return null
    if(typeof db === 'object' && typeof db.hash === 'string' && typeof db.salt === 'string')
      return db
    return null
  }
}

module.exports = Takatan.register(PasswordField)
