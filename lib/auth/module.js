const { Takatan, Log, Kefir } = require('@takatanjs/core')
const log = Log.m('AuthModule')

const crypto = require('crypto')

const PasswordField = require('./field.password')

class AuthModule extends Takatan.extends('AbstractCoreModule')
{

  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('field').factoryRegisterClass('field_type',PasswordField)
      this.obsEnd(obs)
    })
  }

  genSalt()
  {
    return Math.random().toString(16).slice(2)
  }

  salt(msg,withSalt=null)
  {
    let hash = crypto.createHash('sha256')
    hash.update(msg)
    let hash1 = hash.digest('hex')
    if (withSalt)
      hash1 += withSalt
    hash1 += this.moduleConfig().secret
    let hash2 = crypto.createHash('sha256')
    hash2.update(hash1)
    return hash2.digest('hex')
  }

  saltVerify(msg,salt,withSalt=null)
  {
    let hash = this.salt(msg,withSalt)
    return (hash === salt)
  }

  userAccess(user)
  {
    return Kefir.stream(obs=>{
      //log.test('populating user access',user.titles())
      let ugm = this.C('usersgroup')._model
      let rlm = this.C('role')._model
      let q = ugm.find({$or:[{users:user.id()},{code:'guests'}]}).populate({
        path: 'roles',
        model: rlm
      })
      Kefir.fromPromise(q.exec()).observe(groups=>{
        //console.log(['ACCESS user access',user.titles(),groups])
        let roles = []
        let grps = []
        groups.forEach(g=>{
          grps.push(g.code[0])
          if (g&&Array.isArray(g.roles))
            g.roles.forEach(r=>{
              roles.push(this.C('role').loadNode(r))
            })
        })
        this.obsEnd(obs,{roles:roles,gcodes:grps})
      },err=>{
        log.error('userAccess: search groups fail',err)
        console.log(err)
        this.obsErr(obs,err)
      })
    })
  }
  /*


  guestUsers(queryOpts)
  {
    return Rx.Observable.create(obs=>{
      this.C('usersgroup').findNodes({}).subscribe($groups=>{
        let nonguests = []
        $groups.forEach($g=>{
          $g.field('users').all().forEach(uid=>nonguests.push(uid))
        })
        this.C('user').findNodes({_id:{$nin:nonguests}},queryOpts).subscribe($guests=>{
          this.obsEnd(obs,$guests)
        },err=>this.obsErr(obs,err))
      },err=>this.obsErr(obs,err))
    })
  }


  */

    /*
  _genGuestUser()
  {
    return Rx.Observable.create(obs=>{
      this.C('user').initNode().subscribe(guest=>{
        guest.isGuest = true
        obs.next(guest)
        obs.complete()
      })
    })
  }
    */

  updateAdminsAccess()
  {
    return this.C('usersgroup').findNode({code:'admins'})
    .flatMap($grp=>this.C('role').findNodes({})
      .flatMap($roles=>{
        log.info('updating admins access. roles:',$roles.length)
        $grp.field('roles').all($roles)
        return $grp.save()
      })
    )
  }


  moduleInit()
  {
    return this.updateAdminsAccess()
    .flatMap(()=>this.C('usersgroup').findNode({code:'admins'},{populate:1}))
    .map(adm=>{
      for(let i=0;i<adm.field('roles').size();i++)
      {
        let $rl = adm.field('roles').getp(i)
        log.info('admins have role',$rl.role())
      }
    })
    .flatMap(()=>this.C('user').findNode({username:'root'}))
    .map($root=>{
      let def = $root.field('password').verify('1234')
      //log.test('testing default root password',def)
      if (def)
        log.warn('CHANGE DEFAULT ROOT PASSWORD!')
      return true
    })
  }

  moduleInstall()
  {
    //module config
    return Kefir.constant({
      secret: this.genSalt()
    })
  }

  /**/
  moduleDefaults()
  {
    return Kefir.stream(obs=>{
      log.info('installing auth data')

      let $t = []

      log.info('installing roles')
      $t.push(this.M('role').roleSave('core','root','Absolute administrator'))
      $t.push(this.M('role').roleSave('auth','root','Users administrator'))

      log.info('installing users groups')
      log.info('admins users groups')
      let grpAdmins = this.C('usersgroup').newNode()
      grpAdmins.f('code','admins')
      grpAdmins.f('name','Administrators')
      grpAdmins.f('description','System administrators')
      grpAdmins.f('isSystem',true)
      $t.push(grpAdmins.save())

      log.info('guests users groups')
      let grpGuests = this.C('usersgroup').newNode()
      grpGuests.f('code','guests')
      grpGuests.f('name','Guests')
      grpGuests.f('isSystem',true)
      grpGuests.f('description','System guests')
      $t.push(grpGuests.save())

      log.info('creating root user')
      let root = this.C('user').newNode()
      root.f('username','root')
      root.f('password','1234')
      $t.push(root.save().flatMap($root=>{
        log.test('testing root password',$root.field('password').verify('1234'))
        log.info('pushing root user to admins group')
        grpAdmins.field('users').push($root,{unique:true})
        return grpAdmins.save()
      }).map(_=>{
        log.success('root user is admin now')
        return _
      }))

      this.obsEnd(obs,$t)
    })
    .flatMap(x=>Kefir.concat(x))
    .last()
    .flatMap(()=>this.updateAdminsAccess())
    .map(()=>{
      log.success('auth defaults installed')
      return true
    })

  }

  /**/


}

module.exports = Takatan.register(AuthModule)
